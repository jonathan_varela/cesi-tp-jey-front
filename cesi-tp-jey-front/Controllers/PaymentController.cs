﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Web.Mvc;
using cesi_tp_jey_front.Services;
using Newtonsoft.Json;
using cesi_tp_jey_front.Models;
using cesi_tp_jey_front.DTO;

namespace cesi_tp_jey_front.Controllers
{
    
    /// <summary>
    /// This controller redirect the paths towards services and views for payments.
    /// </summary>
    public class PaymentController : Controller
    {
        
        /// <summary>
        /// This method initialize WebClient for sending resources with URI to service.
        /// </summary>
        /// <returns>Payments list</returns>
        [HttpGet]
        public ActionResult List()
        {
            
            using (var client = new WebClient())
            {
                // TODO : Implement DI.
                PaymentServiceImpl paymentService = new PaymentServiceImpl();               
                return View("List", paymentService.GetAll(client));
            }
        }
   
        [HttpGet]
        public ActionResult Create()
        {            
                return View("Create");
        }

        /// <summary>
        /// Add the date to the resource. Verify and send it to paymentService
        /// </summary>
        /// <param name="payment">A new payment.</param>
        /// <returns>If success redirect to list
        /// If error redirect to view with error message</returns>
        [HttpPost]
        public ActionResult Create(Payment payment)
        {
            // if Model is valid redirect a view with a payments list
            // else return payment view with an error message
            if (ModelState.IsValid)
            {
                payment.creationDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                PaymentServiceImpl paymentService = new PaymentServiceImpl();
                
                if (paymentService.Create(payment))
                {
                    return RedirectToAction("List");
                }
                else
                {
                    ViewData["error"] = "Amount can't be negative value";
                    return View(payment);
                }                
            }
            else
            {
                return View(payment);
            }

        }      

    }

}