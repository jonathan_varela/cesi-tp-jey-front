﻿using AutoMapper;
using cesi_tp_jey_front.DTO;
using cesi_tp_jey_front.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace cesi_tp_jey_front.Services
{

    /// <summary>
    /// This service receives ressources from controllers and MiddleEnd
    /// </summary>
    public class PaymentServiceImpl :IPaymentService
    {

        /// <summary>
        /// Initialize RestRequest to do post resquest
        /// Convert the object to a json string
        /// Add the json to the request
        /// Send the request
        /// </summary>
        /// <param name="payment">Payment from the controller</param>
        /// <returns>A bool is the sending is succeful</returns>
        public bool Create(Payment payment)
        {
            // TODO : Replace the object by its DTO
            RestClient client = new RestClient();
            var request = new RestRequest("http://localhost:8090/api/payment-orders", Method.POST);

            request.Parameters.Clear();

            string json = JsonConvert.SerializeObject(payment);

            request.AddParameter("application/json", json, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);

            return response.IsSuccessful;

        }

        /// <summary>
        /// Define the uri
        /// Get the json from request to DTO list
        /// Replace DTO by model Entity
        /// </summary>
        /// <param name="client">Web object from controller to do post request</param>
        /// <returns>Payments list</returns>
        public List<Payment> GetAll(WebClient client)
        {
            // TODO : Add mapper from DTO to Model
            var url = new UriBuilder("http://localhost:8090/api/payment-orders");

            string json = client.DownloadString(url.ToString());

            List<PaymentDTO> paymentDTOs = JsonConvert.DeserializeObject<List<PaymentDTO>>(json);

            List<Payment> payments = new List<Payment>();

            foreach (PaymentDTO paymentDTO in paymentDTOs)
            {
                Payment payment = new Payment();

                payment.id = paymentDTO.id;
                payment.amount = paymentDTO.amount;
                payment.creationDate = paymentDTO.creationDate;
                payment.customerName = paymentDTO.customerName;

                payments.Add(payment);
            }

            return payments;

        }

    }

}