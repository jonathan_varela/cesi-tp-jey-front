﻿using cesi_tp_jey_front.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace cesi_tp_jey_front.Services
{

    interface IPaymentService
    {

        bool Create(Payment payment);

        List<Payment> GetAll(WebClient client);

    }

}
