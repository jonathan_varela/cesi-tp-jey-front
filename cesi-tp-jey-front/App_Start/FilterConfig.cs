﻿using System.Web;
using System.Web.Mvc;

namespace cesi_tp_jey_front
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
