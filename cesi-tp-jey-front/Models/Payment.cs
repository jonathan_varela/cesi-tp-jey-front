﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cesi_tp_jey_front.Models
{

    public class Payment
    {

        private int _id;
        private string _customerName;
        private float _amount;
        private string _creationDate;

        // Getters and Setters

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        [Required]
        public string customerName
        {
            get { return _customerName; }
            set { _customerName = value; }
        }

        [Range(0, Int32.MaxValue, ErrorMessage = "Value should be greater than or equal to 0")]
        public float amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        public string creationDate
        {
            get { return _creationDate; }
            set { _creationDate = value; }
        }

    }

}