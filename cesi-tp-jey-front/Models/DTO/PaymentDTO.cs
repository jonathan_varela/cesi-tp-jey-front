﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cesi_tp_jey_front.DTO 
{

    public class PaymentDTO
    {

        private int _id;
        private string _customerName;
        private float _amount;
        private String _creationDate;

        // Getters and Setters

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string customerName
        {
            get { return _customerName; }
            set { _customerName = value; }
        }

        [Range(0, Int32.MaxValue, ErrorMessage = "Value should be greater than or equal to 1")]
        public float amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        public String creationDate
        {
            get { return _creationDate; }
            set { _creationDate = value; }
        }

    }

}